import { Pipe, PipeTransform } from '@angular/core';
import { stringify } from '@angular/core/src/render3/util';

@Pipe({
  name: 'password'
})
export class PasswordPipe implements PipeTransform {

  transform(value: string, activar: boolean = true): any {
    let pass = '';
    if (activar) {
      for (let i = 0; i < value.length; i++) {
        pass += '*';
      }
    } else {
      pass = value;
    }
    return pass;
  }

}
